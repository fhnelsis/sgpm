<!doctype html>
<html lang='pt'>
<head>
<meta charset='utf-8'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/menu.css">
<!--<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>-->
<script src="js/menu.js"></script>

</head>
<body>

	<div id='cssmenu'>
		<ul>
			<li><a href='home.php'>Home</a></li>

			<li class='active'><a href='#'>Atendimentos</a>
				<ul>
					<li><a href='consultarAtendimento.php'>Buscar Atedimento</a></li>
					<li><a href='formAtendimento.php'>Novo Atendimento</a>
					
					<li><a href='consultarTipoAtendimento.php'>Tipos de Atendimento</a>
					
					<li><a href='formTipoAtendimento.php'>Novo Tipo de Atendimento</a>
				
				</ul></li>

			<li class='active'><a href='#'>Pacientes</a>
				<ul>
					<li><a href='consultarPaciente.php'>Buscar Paciente</a></li>
					<li><a href='formPaciente.php'>Novo Paciente</a>
				
				</ul></li>

			<li class='active'><a href='#'>Funcionários</a>
				<ul>
					<li><a href='consultarFuncionario.php'>Buscar Funcionário</a></li>
					<li><a href='formFuncionario.php'>Novo Funcionário</a></li>
				</ul></li>
			<li class='active'><a href='#'>Relatórios</a>
				<ul>
					<li><a href='#'>Relatórios Administrativos</a></li>
					<li><a href='#'>Relatórios Médicos</a></li>
				</ul></li>
			<li><a href='#'>Sobre</a></li>
			<li><a href="mailto:fhnelsis@outlook.com" target="_top">Contato</a></li>
		</ul>
	</div>
</body>
<html>