<?php session_start(); ?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SGPM - Sistema Gerenciador de Prontuários Médicos</title>
<LINK href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/dataTables.css">

<script type="text/javascript" language="javascript"
	src="js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" language="javascript"
	src="js/dataTables.js"></script>
</head>

<body>
	<div class="cabecalho">
		<a href="home.php"> <img src="img/sgpm_logo2_fundoless_pequeno.png"
			class="logo">
		</a>
		<div class="wrapLogoutButton">
			<a class="logoutButton" href="logout.php">Logout</a>
		</div>
	</div>